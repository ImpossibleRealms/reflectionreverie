...I sense an unfamiliar precense.

You really shouldn't be sticking your nose in places where it doesn't belong, you know.

Regardless, it doesn't matter. The events of this world have already been set into motion. I'm sure we'll meet each other very, very soon.