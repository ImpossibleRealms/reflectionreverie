extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var interactMode = 0
export var dialog = [0]
var canInteract = false
var inRange = false
var framesSinceLastPress = 1
export(Texture) var sprite setget loadSprite
var progress = 0

func loadSprite(tex):
	sprite = tex
	if Engine.editor_hint:
		get_node("Sprite").texture = sprite

# Called when the node enters the scene tree for the first time.
func _ready():
	get_node("Sprite").texture = sprite
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(Input.is_action_just_pressed("ui_accept")):
		framesSinceLastPress = 0
	
	if(canInteract && Main.playerCanMove && inRange):
		match(interactMode):
			0:
				if(Input.is_action_just_pressed("ui_accept")):
					canInteract = false
					if(progress >= dialog.size()):
						Main.createDialog(dialog[dialog.size()-1])
					else:
						Main.createDialog(dialog[progress])
						progress+=1
				
			1:
				if(Input.is_action_just_pressed("ui_accept")):
					canInteract = false
					Main.openChest(dialog[0], progress, self, dialog[1])
			2:
				canInteract = false
				Main.playerCanMove=false
				Main.startBattle()
				queue_free()
	elif(Main.playerCanMove):
		if(interactMode == 2):
			pass
		else:
			if(Input.is_action_just_released("ui_accept")):
				canInteract = true
	
	framesSinceLastPress += 1
	#if(interactMode == 0):
		
#	pass
