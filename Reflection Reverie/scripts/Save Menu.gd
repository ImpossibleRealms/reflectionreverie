extends TileMap


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var isSaving = false
var currentItem = 0

var cursorLocations = [Vector2(7, 20), Vector2(7, 60), Vector2(7, 100), Vector2(39, 136)]
var normalCursorPosition
var cursorTimer = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	normalCursorPosition = cursorLocations[0]
	
	var directory = Directory.new();
	if(!directory.file_exists("user://saves")):
		directory.make_dir("user://saves")
	
	get_node("Save 0/Location").add_font_override("font", Main.font1)
	get_node("Save 0/Play Time").add_font_override("font", Main.font1)
	get_node("Save 0/Level").add_font_override("font", Main.font1)
	
	get_node("Save 1/Location").add_font_override("font", Main.font1)
	get_node("Save 1/Play Time").add_font_override("font", Main.font1)
	get_node("Save 1/Level").add_font_override("font", Main.font1)
	
	get_node("Save 2/Location").add_font_override("font", Main.font1)
	get_node("Save 2/Play Time").add_font_override("font", Main.font1)
	get_node("Save 2/Level").add_font_override("font", Main.font1)
	
	get_node("Back").add_font_override("font", Main.font1)
	
	for i in range(3):
		loadFile(i)
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	cursorTimer += delta
	
	if(Input.is_action_just_pressed("ui_down")):
		currentItem += 1
		if(currentItem >= cursorLocations.size()):
			currentItem = 0
		moveCursor(cursorLocations[currentItem])
	elif(Input.is_action_just_pressed("ui_up")):
		currentItem -= 1
		if(currentItem < 0):
			currentItem = cursorLocations.size()-1
		moveCursor(cursorLocations[currentItem])
	
	if(Input.is_action_just_pressed("ui_accept")):
		match(currentItem):
			3:
				Main.playerCanMove = true
				queue_free()
			_:
				if(!isSaving):
					loadSave(currentItem)
				else:
					saveFile(currentItem)
	
	animateCursor()
	pass

func moveCursor(var pos):
	get_node("Pointer").position = pos
	normalCursorPosition = pos

func animateCursor():
	var check = fmod(cursorTimer, 1.0)
	
	if(check <= 0.25):
		get_node("Pointer").position = normalCursorPosition
	
	if((check >= 0.26 && check <= 0.50) || (check >= 0.76)):
		get_node("Pointer").position = Vector2(normalCursorPosition.x-1, normalCursorPosition.y)
		
	if(check >= 0.51 && check <= 0.75):
		get_node("Pointer").position = Vector2(normalCursorPosition.x-2, normalCursorPosition.y)

func loadFile(var ID):
	var file = File.new()
	if(file.file_exists("user://saves/save" + String(ID) + ".sav")):
		file.open("user://saves/save" + String(ID) + ".sav", File.READ)
		var version = (file.get_8() & 0xFF)
		var name = "              "
		for i in range(14):
			name[i] = (file.get_8() & 0xFF)
		get_node("Save " + String(ID) + "/Location").text = name
		var timePlayed = file.get_32()
		var minutesPlayed = (timePlayed / 60)
		var hoursPlayed = (minutesPlayed / 60)
		var hoursString
		var minutesString
		if(hoursPlayed < 10):
			hoursString = "0" + String(hoursPlayed)
		else:
			hoursString = String(hoursPlayed)
		
		if(minutesPlayed % 60 < 10):
			minutesString = "0" + String(minutesPlayed % 60)
		else:
			minutesString = String(minutesPlayed % 60)
			
		get_node("Save " + String(ID) + "/Play Time").text = hoursString + ":" + minutesString
		var currentRoom = file.get_32()
		file.close()

func loadSave(var ID):
	var file = File.new()
	if(file.file_exists("user://saves/save" + String(ID) + ".sav")):
		file.open("user://saves/save" + String(ID) + ".sav", File.READ)
		var version = (file.get_8() & 0xFF)
		for i in range(14):
			file.get_8()
		#get_node("Save " + String(ID) + "/Location").text = name
		Main.savedTime = file.get_32()
		var currentRoom = file.get_32()
		var playerPosition = Vector2(file.get_float(), file.get_float())
		Main.loadRoom(currentRoom, playerPosition)
		file.close()
		Main.playerCanMove = true
		Main.musicPlayer.playing = true
		queue_free()

func saveFile(var ID):
	var file = File.new()
	#if(file.file_exists("user://saves/save" + String(ID) + ".sav")):
	file.open("user://saves/save" + String(ID) + ".sav", File.WRITE)
	file.store_8(0)
	var nameASCII = Main.roomNames[Main.currentRoom].to_ascii()
	var space = " a"
	for i in range (14):
		if(i < nameASCII.size()):
			file.store_8(nameASCII[i])
			#print("Name [" + String(i) + "] is: " + String(nameASCII[i]))
		else:
			file.store_8(0x20)
	file.store_32((Main.currentTime - Main.startTime) + Main.savedTime)
	file.store_32(Main.currentRoom)
	file.store_float(get_tree().get_current_scene().get_node("OverworldPlayer").position.x)
	file.store_float(get_tree().get_current_scene().get_node("OverworldPlayer").position.y)
	file.store_32(Main.storyProgress)
	file.close()
	Main.playerCanMove = true
	queue_free()
	pass
