extends Spell


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func cast(var target, var caster):
	target.HP += ((caster.specialAttack*(1.5+level))+(target.specialDefense/2))
	if(target.HP > target.maxHP):
		target.HP = target.maxHP
		var notif = BattleLabel.new(Main.font1, "MAX!", target.position)
		Main.battle.add_child(notif)
	else:
		var notif = BattleLabel.new(Main.font1, "+" + String(((caster.specialAttack*1.5)+(target.specialDefense/2))), target.position)
		Main.battle.add_child(notif)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
