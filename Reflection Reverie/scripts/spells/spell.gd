extends Node

class_name Spell

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var mainObj

var cost
var spellName
var level = 0
var nightmare = false

var targetParty = false

func _init(var spellName:String="Missing No.", var cost:int=5, var targetParty:bool=false):
	self.name = spellName
	self.spellName = spellName
	self.cost = cost
	self.targetParty = targetParty
	pass

func setup(var spellName:String="Missing No.", var cost:int=5, var targetParty:bool=false):
	self.name = spellName
	self.spellName = spellName
	self.cost = cost
	self.targetParty = targetParty
	pass


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func cast(var target, var caster):
	pass



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
