extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var partyID = 0
var unlockLV = 1
var spellID = 0
var nightmareRequired = false

func setup(var partyID, var unlockLV, var spellID, var nightmareRequired):
	self.partyID = partyID
	self.unlockLV = unlockLV
	self.spellID = spellID
	self.nightmareRequired = nightmareRequired
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
