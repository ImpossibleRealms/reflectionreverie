extends Node2D

class_name BattleEntity

var HP
var maxHP
var TP
var maxTP
var attack
var baseAttack
var defense
var baseDefense
var specialAttack
var baseSpecialAttack
var specialDefense
var baseSpecialDefense
var cursorOffset

var entName

var sprite = ""

#var font

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _init(var entName:String="Missing No.", HP:int=5, TP:int=5, attack:int=5, defense:int=5, specialAttack:int=5, specialDefense:int=5, cursorOffsetX:int=-8, cursorOffsetY:int=12):
	self.entName = entName
	self.HP = HP
	self.maxHP = HP
	self.TP = TP
	self.maxTP = TP
	self.attack = attack
	self.baseAttack = attack
	self.defense = defense
	self.baseDefense = defense
	self.specialAttack = specialAttack
	self.baseSpecialAttack = specialAttack
	self.specialDefense = specialDefense
	self.baseSpecialDefense = specialDefense
	self.cursorOffset = Vector2(cursorOffsetX, cursorOffsetY)
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func counter(var damage, var special):
	var hit = 0
	
	if(special):
		hit=round(calcSpecialDamage(damage))
	else:
		hit=round(calcDamage(damage))
	
	HP -= hit
	if(HP<0):
		flee()
	return hit

func damage(var damage, var special):
	var hit = 0
	
	if(special):
		hit=round(calcSpecialDamage(damage))
	else:
		hit=round(calcDamage(damage))
	
	HP -= hit
	#print(hit)
	var notif = BattleLabel.new(Main.font1, String(hit), position)
	Main.battle.add_child(notif)
	if(HP<0):
		flee()

func fatalDamage(var damage, var special):
	var hit = 0
	
	if(special):
		hit=round(calcSpecialDamage(damage))
	else:
		hit=round(calcDamage(damage))
	
	HP -= hit
	#print(hit)
	var notif = BattleLabel.new(Main.font1, String(hit), position)
	Main.battle.add_child(notif)
	if(HP<0):
		flee()

func calcDamage(var incomingDamage):
	return 2 * (incomingDamage*incomingDamage)/(incomingDamage+defense)
	#return incomingDamage*(100/(100+defense))

func calcSpecialDamage(var incomingDamage):
	return 2 * (incomingDamage*incomingDamage)/(incomingDamage+specialDefense)

func flee():
	self.visible=false

func die():
	Main.battle.enemies.erase(self)
	self.queue_free()

func turnStart():
	var inc = 1
	if(HP<=0):
		die()
		inc=0
	else:
		self.visible = true
	return inc
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
