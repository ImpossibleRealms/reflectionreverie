extends EnemyTurnEntity


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var chunksSpawned
var chunkObject

# Called when the node enters the scene tree for the first time.
func _ready():
	chunkObject = load("res://prefabs/Attacks/Ground Chunk.tscn")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(position.y <= 136):
		position.y == 136
	else:
		position.y -= 1
		if(position.y<152 && !chunksSpawned):
			chunksSpawned = true
			var chunk = chunkObject.instance()
			chunk.movingLeft = true
			chunk.position = Vector2(position.x, position.y-7)
			chunk.representing = representing
			get_parent().add_child(chunk)
			chunk = chunkObject.instance()
			chunk.position = Vector2(position.x, position.y-7)
			chunk.representing = representing
			get_parent().add_child(chunk)
	pass
