extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var warningTimer = 0
var spawnTimer = 1

var spawn
var spawnInto

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	warningTimer += delta
	spawnTimer -= delta
	
	if(fmod(warningTimer, 0.4) > 0.2):
		region_rect = Rect2(16, 0, 16, 16)
	else:
		region_rect = Rect2(0, 0, 16, 16)
	pass
	
	if(spawnTimer <= 0):
		spawnInto.add_child(spawn)
		queue_free()
