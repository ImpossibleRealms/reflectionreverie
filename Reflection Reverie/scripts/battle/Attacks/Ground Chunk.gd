extends EnemyTurnEntity

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var movingLeft = false
var rotateTimer = 0
var sprite


var velocity = Vector2(0.5, -2)

# Called when the node enters the scene tree for the first time.
func _ready():
	attack = true
	sprite = get_node("Sprite")
	rotateTimer = Main.random.randfn(0.0, 0.4)
	
	if(movingLeft):
		velocity.x = velocity.x * -1
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotateTimer += delta
	
	if(fmod(rotateTimer, 0.4) > 0.1 && fmod(rotateTimer, 0.4) <= 0.2):
		sprite.flip_h=true
		sprite.flip_v=false
	elif(fmod(rotateTimer, 0.4) > 0.2 && fmod(rotateTimer, 0.4) <= 0.3):
		sprite.flip_h=true
		sprite.flip_v=true
	elif(fmod(rotateTimer, 0.4) > 0.3 && fmod(rotateTimer, 0.4) <= 0.4):
		sprite.flip_h=false
		sprite.flip_v=true
	else:
		sprite.flip_h=false
		sprite.flip_v=false
	pass
	
	velocity.y = velocity.y + entGravity
	
	position = position + velocity
