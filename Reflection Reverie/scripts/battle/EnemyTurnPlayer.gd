extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var gravity = 0.1
var jumpforce = 3
var velocity = Vector2(0,0)

var moveTimer = 0

var sprite

var canJump = false

var represents

# Called when the node enters the scene tree for the first time.
func _ready():
	sprite = get_node("Sprite")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	if(Input.is_action_pressed("ui_left") && !Input.is_action_pressed("ui_right")):
		moveTimer += delta
		velocity.x = -1
		sprite.flip_h = true
	
	if(!Input.is_action_pressed("ui_left") && Input.is_action_pressed("ui_right")):
		moveTimer += delta
		velocity.x = 1
		sprite.flip_h = false
	
	if((Input.is_action_pressed("ui_left") && Input.is_action_pressed("ui_right")) || (!Input.is_action_pressed("ui_left") && !Input.is_action_pressed("ui_right"))):
		velocity.x = 0
		moveTimer = 0
	
	if(Input.is_action_just_pressed("ui_accept") && canJump):
		velocity.y = -jumpforce
		canJump=false
	
	velocity.y+=gravity
	
	position += velocity
	
	if(position.x<5):
		position.x = 5
	
	if(position.x>155):
		position.x = 155
	
	if(position.y>=136):
		canJump=true
		position.y=136
		velocity.y=0
	
	if(position.y<136):
		sprite.region_rect = Rect2(16, 0, 16, 16)
	elif(fmod(moveTimer, 0.2) > 0.1):
		sprite.region_rect = Rect2(16, 0, 16, 16)
	else:
		sprite.region_rect = Rect2(0, 0, 16, 16)
	
	pass
