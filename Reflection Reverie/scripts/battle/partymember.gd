extends 'res://scripts/battle/battleentity.gd'

class_name PartyMember

var LV
var action=0
var target=0

var battleMain

var defending = true

var spells = []
var spellChoice = 0

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func _init(var entName:String="Missing No.", LV:int=1, HP:int=5, TP:int=5, attack:int=5, defense:int=5, specialAttack:int=5, specialDefense:int=5, cursorOffsetX:int=-8, cursorOffsetY:int=12):
	self.entName = entName
	self.LV = LV
	self.HP = HP
	self.maxHP = HP
	self.TP = TP
	self.maxTP = TP
	self.attack = attack
	self.baseAttack = attack
	self.defense = defense
	self.baseDefense = defense
	self.specialAttack = specialAttack
	self.baseSpecialAttack = specialAttack
	self.specialDefense = specialDefense
	self.baseSpecialDefense = specialDefense
	self.cursorOffset = Vector2(cursorOffsetX, cursorOffsetY)
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func takeAction():
	if(HP <= 0):
		var notif = BattleLabel.new(Main.font1, "KOed!", position)
		Main.battle.add_child(notif)
	else:
		match(action):
			0:
				defending=true
				var notif = BattleLabel.new(Main.font1, "Defend!", position)
				Main.battle.add_child(notif)
				pass
			1:
				defending=false
				battleMain.enemies[target].damage(attack, false)
			2:
				defending=false
				if(spells[spellChoice].targetParty):
					spells[spellChoice].cast(Main.battle.partymembers[target], self)
				else:
					spells[spellChoice].cast(Main.battle.enemies[target], self)
			3:
				defending=false
				if(Main.items[spellChoice].targetParty):
					#print("Target is " + String(target))
					#print(Main.activePartyMembers.size())
					Main.items[spellChoice].use(Main.activePartyMembers[target])
				else:
					Main.items[spellChoice].use(Main.battle.enemies[target])
			4:
				defending=false
				if(!Main.battle.canFlee):
					var notif = BattleLabel.new(Main.font1, "Failed!", position)
					Main.battle.add_child(notif)
				else:
					var fleeOdds = (randi() % 100) + 1
					if(fleeOdds < Main.battle.fleeChance):
						var notif = BattleLabel.new(Main.font1, "Book it!", position)
						Main.battle.add_child(notif)
						Main.battle.running = true
						Main.battle.endingBattle = true
						Main.battle.get_node("MenuBG/BattleStatus").text = "The party flees!"
						Main.battle.get_node("MenuBG/BattleStatus").visible = true
					else:
						var notif = BattleLabel.new(Main.font1, "Failed!", position)
						Main.battle.add_child(notif)
			_:
				defending=true
				var notif = BattleLabel.new(Main.font1, "Defend!", position)
				Main.battle.add_child(notif)
				pass
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
