extends Label

class_name BattleLabel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var timer = 0
var BattleMain

func _init(var font:BitmapFont=BitmapFont.new(), var text:String="0", var position:Vector2=Vector2(0,0)):
	self.text = text
	self.rect_position = position
	#print(font)
	#print(text)
	add_font_override("font", font)
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	timer+=delta
	if(timer>=1):
		Main.battle.acting=false
		queue_free()
	
	rect_position = Vector2(rect_position.x, rect_position.y-0.20)
	pass
