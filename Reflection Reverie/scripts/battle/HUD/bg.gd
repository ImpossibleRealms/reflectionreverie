extends TileMap


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var moveTimer = 0
var dirTimer = 0
var flipTimer = 0
var flipping = 0
var flipFrame = 0

var velX = -1
var velY = -1

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	moveTimer+=delta
	flipTimer+=delta
	#dirTimer+=delta
	
	if(moveTimer >= 0.05):
		moveTimer -= 0.05
		move(velX, velY)
	
	if(flipTimer >= 3):
		flipTimer -=3
		flipping = true
	
	if(flipping):
		if((flipFrame>=0 && flipFrame<=19) || (flipFrame>=40 && flipFrame<=59)):
			for i in range (-1, 21):
				for j in range (-1, 19):
					set_cell(i, j, 1)
			flipFrame+=1
		
		if(flipFrame>=20 && flipFrame<=39):
			for i in range (-1, 21):
				for j in range (-1, 19):
					set_cell(i, j, 2)
			flipFrame+=1
		
		if(flipFrame>=60):
			for i in range (-1, 21):
				for j in range (-1, 19):
					set_cell(i, j, 0)
			flipping=false
			flipFrame=0
	pass

func _fixed_process(delta):
	pass


func move(var x, var y):
	self.position = Vector2(position.x+x, position.y+y);
	if(self.position.x <= -8 || self.position.x >= 8):
		position = Vector2(0, position.y);
	if(self.position.y <= -8 || self.position.y >= 8):
		position = Vector2(position.x, 0);
	pass
