extends Area2D

class_name EnemyTurnEntity

var representing

var entGravity = 0.1
var attack = false

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	set_monitoring(true)
	connect("body_entered", self, "objectDetected")
	pass # Replace with function body.

func objectDetected(var body):
	if(body.name == "EnemyTurnPlayer"):
		if(attack):
			body.represents.damage(representing.attack, false)
			queue_free()
		else:
			body.velocity.y = -body.jumpforce
			var notif = BattleLabel.new(Main.font1, String(representing.counter(body.represents.attack, false)), position)
			Main.battle.add_child(notif)
			#representing.counter(body.represents.attack, false)
	pass
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
