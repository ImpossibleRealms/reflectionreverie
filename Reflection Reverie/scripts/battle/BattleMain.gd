extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var inBattle = true

var battleplayer = load("res://prefabs/EnemyTurnPlayer.tscn")
var batPlayer
var battlePlayerSpawned = false
var returnedToPlayer = false

var menuOption = 0
var menu = 0

var partyAttacking=false
var attackingStage = 0
var enemyTurn=false
var enemyTurnTimer
var partyTurn=0
var battleTurn=0

var activeAction=0
var acting = false

var enemies = []

var miscOptions = []

var partyMenuPos = [48, 8, 96]

var menu_0_offset_list = [Vector2(16, 120), Vector2(80, 120), Vector2(16, 128), Vector2(80, 128)]

var normalCursorPosition

onready var viewport = get_viewport()

var moveCursorPos = false

var cursorTimer = 0

var endingBattle = false

var transitioning = false
var transitionTimer = 0

var running = false
var canFlee = true
var fleeChance = 100

#hacky system, needs something more elegant, but later, ABSOLUTELY later
var pansySpawnTimer = 0.5
var pansyAttackClass = load("res://prefabs/Attacks/Pansy.tscn")
var warningClass = load("res://prefabs/Attacks/Warning.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	
	if(Main.battleType == 1):
		get_node("BattleBG").tile_set.tile_set_texture(0, load("images/battle/HUD/minibossbattlebg.png"))
		get_node("BattleBG").tile_set.tile_set_texture(1, load("images/battle/HUD/minibossbattlebg.png"))
		get_node("BattleBG").tile_set.tile_set_texture(2, load("images/battle/HUD/minibossbattlebg.png"))
	elif(Main.battleType == 2):
		get_node("BattleBG").tile_set.tile_set_texture(0, load("images/battle/HUD/bossbattlebg.png"))
		get_node("BattleBG").tile_set.tile_set_texture(1, load("images/battle/HUD/bossbattlebg.png"))
		get_node("BattleBG").tile_set.tile_set_texture(2, load("images/battle/HUD/bossbattlebg.png"))
	
	get_node("MenuBG/Menu0/Fight").add_font_override("font", Main.font1)
	get_node("MenuBG/Menu0/Tech").add_font_override("font", Main.font1)
	get_node("MenuBG/Menu0/Item").add_font_override("font", Main.font1)
	get_node("MenuBG/Menu0/Misc").add_font_override("font", Main.font1)
	get_node("MenuBG/Menu0/PartyBG/PartyName").add_font_override("font", Main.font1)
	get_node("MenuBG/Menu0/PartyBG/PartyNameHP").add_font_override("font", Main.font1)
	get_node("MenuBG/Menu0/PartyBG/PartyNameTech").add_font_override("font", Main.font1)
	get_node("MenuBG/MenuEnemy/EnemyName").add_font_override("font", Main.font1)
	get_node("MenuBG/MenuEnemy/EnemyHP").add_font_override("font", Main.font1)
	get_node("MenuBG/MenuEnemy/EnemyTP").add_font_override("font", Main.font1)
	get_node("MenuBG/BattleStatus").add_font_override("font", Main.font1)
	get_node("MenuBG/MenuSelection/PrevItem").add_font_override("font", Main.font1)
	get_node("MenuBG/MenuSelection/CurrentItem").add_font_override("font", Main.font1)
	get_node("MenuBG/MenuSelection/NextItem").add_font_override("font", Main.font1)
	get_node("MenuBG/MenuParty/PartyName").add_font_override("font", Main.font1)
	get_node("MenuBG/MenuParty/PartyHP").add_font_override("font", Main.font1)
	get_node("MenuBG/MenuParty/PartyTP").add_font_override("font", Main.font1)
	#get_node("Fight").text = "FIGHT"
	
	var miscScr = load("res://scripts/battle/HUD/misc_option.gd")
	var misc = miscScr.new()
	misc.action = 0
	misc.name = "Defend"
	miscOptions.append(misc)
	misc = miscScr.new()
	misc.action = 4
	misc.name = "Flee"
	miscOptions.append(misc)
	
	loadParty()
	
	addEnemy(0, 32, 32)
	addEnemy(0, 80, 32)
	addEnemy(0, 128, 32)
	
	normalCursorPosition = get_node("Pointer").position
	
	get_node("MenuBG/Menu0/PartyBG/PartyName").text = Main.activePartyMembers[partyTurn].entName
	get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
	get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
	get_node("MenuBG/Menu0/PartyBG").rect_position = Vector2(partyMenuPos[partyTurn], -32)
	
	pass # Replace with function body.

func loadParty():
	for i in range(Main.activePartyMembers.size()):
		#var member = Main.activePartyMembers[i]
		var member = PartyMember.new(Main.activePartyMembers[i].entName, Main.activePartyMembers[i].LV, Main.activePartyMembers[i].HP, Main.activePartyMembers[i].TP, Main.activePartyMembers[i].attack, Main.activePartyMembers[i].defense, Main.activePartyMembers[i].specialAttack, Main.activePartyMembers[i].specialDefense, Main.activePartyMembers[i].cursorOffset.x, Main.activePartyMembers[i].cursorOffset.y)
		member.name = String(i)
		member.sprite = Main.activePartyMembers[i].sprite
		#var member = PartyMember.new()
		member.battleMain = self
		Main.activePartyMembers[i].battleMain = self
		var placeholder = Sprite.new()
		match(i):
			1:
				member.position = Vector2(16, 80)
				Main.activePartyMembers[i].position = Vector2(16, 80)
				placeholder.flip_h = true
			2:
				member.position = Vector2(128, 80)
				Main.activePartyMembers[i].position = Vector2(128, 80)
			_:
				member.position = Vector2(72, 84)
				Main.activePartyMembers[i].position = Vector2(72, 84)
		placeholder.name = "BattleSprite"
		placeholder.centered = false
		var tex = load("res://images/battle/Party/" + member.sprite)
		#var tex = load("res://images/battle/Party/David.png")
		placeholder.texture = tex
		#print("res://images/battle/Party/" + member.sprite)
		#print(placeholder.texture)
		#placeholder.color = Color(0.203922, 0.407843, 0.337255)
		#placeholder.rect_size = Vector2(16,24)
		member.add_child(placeholder)
		get_node("Party").add_child(member)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	cursorTimer += delta
	
	if(endingBattle):
		Main.playerCanMove = true
		Main.inBattle = false
		Main.endBattle()
		pass
	elif(enemyTurn):
		if(transitioning):
			transitionTimer+=delta
			
			if(transitionTimer <=0.5):
				get_node("BattleTransition/Top").position = Vector2(0, 72*(transitionTimer*2))
				get_node("BattleTransition/Bottom").position = Vector2(0, 145-(72*(transitionTimer*2)))
			elif(transitionTimer > 0.5 && transitionTimer <= 0.7):
				#get_node("EnemyTurn").remove_child(batPlayer)
				#batPlayer.queue_free()
				if(!returnedToPlayer):
					returnedToPlayer = true
					
					get_node("Party").visible = true
					
					get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
					get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
					
					for i in range(0, get_node("EnemyTurn").get_child_count()):
						get_node("EnemyTurn").get_child(i).queue_free()
					#enemyTurn=false
					get_node("BattleBG").visible=true
					get_node("MenuBG").visible=true
					get_node("Pointer").visible=true
					get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/BattleStatus").visible=false
					
					battlePlayerSpawned = false
					
					var i=0
					while(i<enemies.size()):
						i+=enemies[i].turnStart()
					
					for partyMember in Main.activePartyMembers:
						partyMember.visible=true
					battleTurn+=1
			elif(transitionTimer > 0.7 && transitionTimer <= 1.2):
				get_node("BattleTransition/Top").position = Vector2(0, 71-(72*((transitionTimer-0.7)*2)))
				get_node("BattleTransition/Bottom").position = Vector2(0, 73+(72*((transitionTimer-0.7)*2)))
			else:
				transitioning=false
				transitionTimer=0
				enemyTurn = false
		else:
			enemyTurnTimer-=delta
			pansySpawnTimer-=delta
			if(enemyTurnTimer<=0):
				transitioning=true
				transitionTimer=0
			#Okay, this is also super hacky, but I've gotta get this done, lol
			elif(pansySpawnTimer<=0):
				pansySpawnTimer = 99999
				var pansy = pansyAttackClass.instance()
				pansy.position = Vector2(get_node("EnemyTurn/EnemyTurnPlayer").position.x, 155)
				#ALSO super hacky
				for enemy in enemies:
					if(enemy.HP > 0):
						pansy.representing = enemy
						break
				var warning = warningClass.instance()
				warning.position = Vector2(get_node("EnemyTurn/EnemyTurnPlayer").position.x, 131)
				warning.spawn = pansy
				warning.spawnInto = get_node("EnemyTurn")
				get_node("EnemyTurn").add_child(warning)
				#pansy.position = Vector2(p)
			
	elif(partyAttacking):
		if(transitioning):
			transitionTimer+=delta
			returnedToPlayer = false
			if(transitionTimer <=0.5):
				get_node("BattleTransition/Top").position = Vector2(0, 72*(transitionTimer*2))
				get_node("BattleTransition/Bottom").position = Vector2(0, 145-(72*(transitionTimer*2)))
			elif(transitionTimer > 0.5 && transitionTimer <= 0.7):
				get_node("BattleBG").visible=false
				get_node("MenuBG").visible=false
				get_node("Pointer").visible=false
				
				for enemy in enemies:
					enemy.visible = false
				
				#for partyMember in Main.activePartyMembers:
				#	partyMember.visible=false
				get_node("Party").visible = false
				
				get_node("BattleTransition/Top").position = Vector2(0, 72)
				get_node("BattleTransition/Bottom").position = Vector2(0, 72)
				if(!battlePlayerSpawned):
					battlePlayerSpawned = true
					batPlayer = battleplayer.instance()
					batPlayer.position = Vector2(80, 136)
					var rep = 0
					for party in Main.activePartyMembers:
						if(party.HP <=0):
							rep += 1
					
					if(rep < Main.activePartyMembers.size()):
						batPlayer.represents = Main.activePartyMembers[rep]
					else:
						for party in Main.activePartyMembers:
							party.HP = party.maxHP
						endingBattle = true
					get_node("EnemyTurn").add_child(batPlayer)
					pansySpawnTimer = 1
				enemyTurnTimer = 5
			elif(transitionTimer > 0.7 && transitionTimer <= 1.2):
				get_node("BattleTransition/Top").position = Vector2(0, 71-(72*((transitionTimer-0.7)*2)))
				get_node("BattleTransition/Bottom").position = Vector2(0, 73+(72*((transitionTimer-0.7)*2)))
			else:
				transitioning=false
				transitionTimer=0
				enemyTurn = true
				partyAttacking=false
				attackingStage=0
				
		elif(!acting):
			if(attackingStage<Main.activePartyMembers.size()):
				acting=true
				Main.activePartyMembers[attackingStage].takeAction()
				attackingStage+=1
			else:
				endingBattle=true
				for enemy in enemies:
					if(enemy.HP>0):
						endingBattle=false
				if(endingBattle):
					get_node("MenuBG/BattleStatus").text = "The enemy flees!"
				else:
					get_node("MenuBG/BattleStatus").text = "The enemy strikes!"
					transitioning = true
	else:
		match(menu):
			0:
				endingBattle = true
				for enemy in enemies:
					if(enemy.HP>0):
						endingBattle=false
				
				var deadCount = 0
				for party in Main.activePartyMembers:
					if(party.HP<=0):
						deadCount += 1
				
				if(deadCount == Main.activePartyMembers.size()):
					endingBattle = true
					for party in Main.activePartyMembers:
						party.HP = party.maxHP
				
				if(Main.activePartyMembers[partyTurn].HP <= 0):
					partyTurn += 1
					get_node("MenuBG/Menu0/PartyBG/PartyName").text = Main.activePartyMembers[partyTurn].entName
					get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
					get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
					get_node("MenuBG/Menu0/PartyBG").rect_position = Vector2(partyMenuPos[partyTurn], -32)
				
				if(endingBattle):
					get_node("MenuBG/BattleStatus").text = "The enemy flees!"
				if(Input.is_action_just_pressed("ui_left") || Input.is_action_just_pressed("ui_right")):
					if(menuOption%2 == 0):
						menuOption+=1;
					else:
						menuOption-=1;
					moveCursorPos=true
				
				if(Input.is_action_just_pressed("ui_up") || Input.is_action_just_pressed("ui_down")):
					if(menuOption/2 == 0):
						menuOption+=2;
					else:
						menuOption-=2;
					moveCursorPos=true
				
				if(moveCursorPos):
					moveCursorPos=false
					if(menuOption<0):
						menuOption=0
					if(menuOption>=menu_0_offset_list.size()):
						menuOption=menu_0_offset_list.size()-1
					moveCursor(menu_0_offset_list[menuOption])
				
				if(Input.is_action_just_pressed("ui_cancel")):
					if(partyTurn > 0):
						partyTurn -= 1
						menuOption=0
						get_node("MenuBG/Menu0/PartyBG/PartyName").text = Main.activePartyMembers[partyTurn].entName
						get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
						get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
						get_node("MenuBG/Menu0/PartyBG").rect_position = Vector2(partyMenuPos[partyTurn], -32)
						moveCursor(menu_0_offset_list[0])
			1:
				if(Input.is_action_just_pressed("ui_left")):
					if(menuOption <= 0):
						menuOption = enemies.size()-1
					else:
						menuOption -= 1
					moveCursorPos=true
				if(Input.is_action_just_pressed("ui_right")):
					if(menuOption >= enemies.size()-1):
						menuOption = 0
					else:
						menuOption += 1
					moveCursorPos=true
				if(moveCursorPos):
					moveCursorPos=false
					if(menuOption<0):
						menuOption=0
					if(menuOption>=enemies.size()):
						menuOption=enemies.size()-1
					moveCursor(Vector2(enemies[menuOption].position.x + enemies[menuOption].cursorOffset.x, enemies[menuOption].position.y + enemies[menuOption].cursorOffset.y))
					get_node("MenuBG/MenuEnemy/EnemyName").text = enemies[menuOption].entName
					get_node("MenuBG/MenuEnemy/EnemyHP").text = String(enemies[menuOption].HP) + "/" + String(enemies[menuOption].maxHP)
					get_node("MenuBG/MenuEnemy/EnemyTP").text = String(enemies[menuOption].TP) + "/" + String(enemies[menuOption].maxTP)
				if(Input.is_action_just_pressed("ui_cancel")):
					menu=0
					menuOption=0
					get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuEnemy").visible=false
					moveCursor(menu_0_offset_list[0])
			2:
				if(Input.is_action_just_pressed("ui_up")):
					menuOption-=1
				elif(Input.is_action_just_pressed("ui_down")):
					menuOption+=1
				
				if(menuOption < 0):
					menuOption = 0
				
				if(menuOption >= Main.activePartyMembers[partyTurn].spells.size()):
					menuOption = Main.activePartyMembers[partyTurn].spells.size()-1
				
				getSelection(Main.activePartyMembers[partyTurn].spells, menuOption)
			3:
				if(Input.is_action_just_pressed("ui_left")):
					if(menuOption <= 0):
						menuOption = enemies.size()-1
					else:
						menuOption -= 1
					moveCursorPos=true
				if(Input.is_action_just_pressed("ui_right")):
					if(menuOption >= enemies.size()-1):
						menuOption = 0
					else:
						menuOption += 1
					moveCursorPos=true
				if(moveCursorPos):
					moveCursorPos=false
					if(menuOption<0):
						menuOption=0
					if(menuOption>=enemies.size()):
						menuOption=enemies.size()-1
					moveCursor(Vector2(enemies[menuOption].position.x + enemies[menuOption].cursorOffset.x, enemies[menuOption].position.y + enemies[menuOption].cursorOffset.y))
					get_node("MenuBG/MenuEnemy/EnemyName").text = enemies[menuOption].entName
					get_node("MenuBG/MenuEnemy/EnemyHP").text = String(enemies[menuOption].HP) + "/" + String(enemies[menuOption].maxHP)
					get_node("MenuBG/MenuEnemy/EnemyTP").text = String(enemies[menuOption].TP) + "/" + String(enemies[menuOption].maxTP)
				if(Input.is_action_just_pressed("ui_cancel")):
					menu=0
					menuOption=1
					get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuEnemy").visible=false
					moveCursor(menu_0_offset_list[1])
			4:
				if(Input.is_action_just_pressed("ui_left")):
					if(menuOption <= 0):
						menuOption = Main.activePartyMembers.size()-1
					else:
						menuOption -= 1
					moveCursorPos=true
				if(Input.is_action_just_pressed("ui_right")):
					if(menuOption >= Main.activePartyMembers.size()-1):
						menuOption = 0
					else:
						menuOption += 1
					moveCursorPos=true
				if(moveCursorPos):
					moveCursorPos=false
					if(menuOption<0):
						menuOption=0
					if(menuOption>=Main.activePartyMembers.size()):
						menuOption=Main.activePartyMembers.size()-1
					moveCursor(Vector2(get_node("Party/" + String(menuOption)).position.x + Main.activePartyMembers[menuOption].cursorOffset.x, get_node("Party/" + String(menuOption)).position.y + Main.activePartyMembers[menuOption].cursorOffset.y))
					get_node("MenuBG/MenuParty/PartyName").text = Main.activePartyMembers[menuOption].entName
					get_node("MenuBG/MenuParty/PartyHP").text = String(Main.activePartyMembers[menuOption].HP) + "/" + String(Main.activePartyMembers[menuOption].maxHP)
					get_node("MenuBG/MenuParty/PartyTP").text = String(Main.activePartyMembers[menuOption].TP) + "/" + String(Main.activePartyMembers[menuOption].maxTP)
				if(Input.is_action_just_pressed("ui_cancel")):
					menu=0
					menuOption=1
					get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuParty").visible=false
					moveCursor(menu_0_offset_list[1])
			5:
				if(Input.is_action_just_pressed("ui_up")):
					menuOption-=1
				elif(Input.is_action_just_pressed("ui_down")):
					menuOption+=1
				
				if(menuOption < 0):
					menuOption = 0
				
				if(menuOption >= Main.items.size()):
					menuOption = Main.items.size()-1
				
				getSelection(Main.items, menuOption)
			6:
				if(Input.is_action_just_pressed("ui_left")):
					if(menuOption <= 0):
						menuOption = enemies.size()-1
					else:
						menuOption -= 1
					moveCursorPos=true
				if(Input.is_action_just_pressed("ui_right")):
					if(menuOption >= enemies.size()-1):
						menuOption = 0
					else:
						menuOption += 1
					moveCursorPos=true
				if(moveCursorPos):
					moveCursorPos=false
					if(menuOption<0):
						menuOption=0
					if(menuOption>=enemies.size()):
						menuOption=enemies.size()-1
					moveCursor(Vector2(enemies[menuOption].position.x + enemies[menuOption].cursorOffset.x, enemies[menuOption].position.y + enemies[menuOption].cursorOffset.y))
					get_node("MenuBG/MenuEnemy/EnemyName").text = enemies[menuOption].entName
					get_node("MenuBG/MenuEnemy/EnemyHP").text = String(enemies[menuOption].HP) + "/" + String(enemies[menuOption].maxHP)
					get_node("MenuBG/MenuEnemy/EnemyTP").text = String(enemies[menuOption].TP) + "/" + String(enemies[menuOption].maxTP)
				if(Input.is_action_just_pressed("ui_cancel")):
					menu=0
					menuOption=2
					get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuEnemy").visible=false
					moveCursor(menu_0_offset_list[2])
			7:
				if(Input.is_action_just_pressed("ui_left")):
					if(menuOption <= 0):
						menuOption = Main.activePartyMembers.size()-1
					else:
						menuOption -= 1
					moveCursorPos=true
				if(Input.is_action_just_pressed("ui_right")):
					if(menuOption >= Main.activePartyMembers.size()-1):
						menuOption = 0
					else:
						menuOption += 1
					moveCursorPos=true
				if(moveCursorPos):
					moveCursorPos=false
					if(menuOption<0):
						menuOption=0
					if(menuOption>=Main.activePartyMembers.size()):
						menuOption=Main.activePartyMembers.size()-1
					moveCursor(Vector2(Main.activePartyMembers[menuOption].position.x + Main.activePartyMembers[menuOption].cursorOffset.x, Main.activePartyMembers[menuOption].position.y + Main.activePartyMembers[menuOption].cursorOffset.y))
					get_node("MenuBG/MenuParty/PartyName").text = Main.activePartyMembers[menuOption].entName
					get_node("MenuBG/MenuParty/PartyHP").text = String(Main.activePartyMembers[menuOption].HP) + "/" + String(Main.activePartyMembers[menuOption].maxHP)
					get_node("MenuBG/MenuParty/PartyTP").text = String(Main.activePartyMembers[menuOption].TP) + "/" + String(Main.activePartyMembers[menuOption].maxTP)
				if(Input.is_action_just_pressed("ui_cancel")):
					menu=0
					menuOption=2
					get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuParty").visible=false
					moveCursor(menu_0_offset_list[2])
			8:
				if(Input.is_action_just_pressed("ui_up")):
					menuOption-=1
				elif(Input.is_action_just_pressed("ui_down")):
					menuOption+=1
				
				if(menuOption < 0):
					menuOption = 0
				
				if(menuOption >= miscOptions.size()):
					menuOption = miscOptions.size()-1
				
				getSelection(miscOptions, menuOption)
			
		if(Input.is_action_just_pressed("ui_accept")):
			match menu:
				0:
					match menuOption:
						0:	
							menu=1
							get_node("MenuBG/Menu0").visible=false
							moveCursor(Vector2(enemies[0].position.x + enemies[0].cursorOffset.x, enemies[0].position.y + enemies[0].cursorOffset.y))
							get_node("MenuBG/MenuEnemy").visible = true
							get_node("MenuBG/MenuEnemy/EnemyName").text = enemies[0].entName
							get_node("MenuBG/MenuEnemy/EnemyHP").text = String(enemies[0].HP) + "/" + String(enemies[0].maxHP)
							get_node("MenuBG/MenuEnemy/EnemyTP").text = String(enemies[0].TP) + "/" + String(enemies[0].maxTP)
						1:
							if(Main.activePartyMembers[partyTurn].spells.size()>0):
								menu=2
								menuOption=0
								get_node("MenuBG/Menu0").visible=false
								get_node("MenuBG/MenuSelection").visible=true
								getSelection(Main.activePartyMembers[partyTurn].spells, menuOption)
								moveCursor(Vector2(8,128))
						2:
							if(Main.items.size()>0):
								menu=5
								menuOption=0
								get_node("MenuBG/Menu0").visible=false
								get_node("MenuBG/MenuSelection").visible=true
								getSelection(Main.items, menuOption)
								moveCursor(Vector2(8,128))
						3:
							if(miscOptions.size()>0):
								#print(miscOptions.size())
								menu=8
								menuOption=0
								get_node("MenuBG/Menu0").visible=false
								get_node("MenuBG/MenuSelection").visible=true
								getSelection(miscOptions, menuOption)
								moveCursor(Vector2(8,128))
						
				1:
					Main.activePartyMembers[partyTurn].action = 1
					Main.activePartyMembers[partyTurn].target = menuOption
					if(partyTurn >= Main.activePartyMembers.size()-1):
						partyAttacking=true
						partyTurn=0
						get_node("Pointer").visible=false
						get_node("MenuBG/BattleStatus").text = "The party strikes!"
						get_node("MenuBG/BattleStatus").visible = true
						
					else:
						partyTurn+=1
						get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuEnemy").visible=false
					menu=0
					menuOption=0
					get_node("MenuBG/Menu0/PartyBG/PartyName").text = Main.activePartyMembers[partyTurn].entName
					get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
					get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
					get_node("MenuBG/Menu0/PartyBG").rect_position = Vector2(partyMenuPos[partyTurn], -32)
					moveCursor(menu_0_offset_list[0])
				2:
					Main.activePartyMembers[partyTurn].spellChoice = menuOption
					if(Main.activePartyMembers[partyTurn].spells[menuOption].targetParty):
						menu=4
						get_node("MenuBG/MenuParty").visible = true
						get_node("MenuBG/MenuParty/PartyName").text = Main.activePartyMembers[0].entName
						get_node("MenuBG/MenuParty/PartyHP").text = String(Main.activePartyMembers[0].HP) + "/" + String(Main.activePartyMembers[0].maxHP)
						get_node("MenuBG/MenuParty/PartyTP").text = String(Main.activePartyMembers[0].TP) + "/" + String(Main.activePartyMembers[0].maxTP)
						moveCursor(Vector2(get_node("Party/" + String(0)).position.x + Main.activePartyMembers[0].cursorOffset.x, get_node("Party/" + String(0)).position.y + Main.activePartyMembers[0].cursorOffset.y))
					else:
						menu=3
						get_node("MenuBG/MenuEnemy").visible = true
						get_node("MenuBG/MenuEnemy/EnemyName").text = enemies[0].entName
						get_node("MenuBG/MenuEnemy/EnemyHP").text = String(enemies[0].HP) + "/" + String(enemies[0].maxHP)
						get_node("MenuBG/MenuEnemy/EnemyTP").text = String(enemies[0].TP) + "/" + String(enemies[0].maxTP)
						moveCursor(Vector2(enemies[0].position.x + enemies[0].cursorOffset.x, enemies[0].position.y + enemies[0].cursorOffset.y))
					menuOption=0
					
					get_node("MenuBG/Menu0").visible=false
					
					get_node("MenuBG/MenuSelection").visible=false
					
				3:
					Main.activePartyMembers[partyTurn].action = 2
					Main.activePartyMembers[partyTurn].target = menuOption
					if(partyTurn >= Main.activePartyMembers.size()-1):
						partyAttacking=true
						partyTurn=0
						get_node("Pointer").visible=false
						get_node("MenuBG/BattleStatus").text = "The party strikes!"
						get_node("MenuBG/BattleStatus").visible = true
						
					else:
						partyTurn+=1
						get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuEnemy").visible=false
					menu=0
					menuOption=0
					get_node("MenuBG/Menu0/PartyBG/PartyName").text = Main.activePartyMembers[partyTurn].entName
					get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
					get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
					get_node("MenuBG/Menu0/PartyBG").rect_position = Vector2(partyMenuPos[partyTurn], -32)
					moveCursor(menu_0_offset_list[0])
				4:
					Main.activePartyMembers[partyTurn].action = 2
					Main.activePartyMembers[partyTurn].target = menuOption
					if(partyTurn >= Main.activePartyMembers.size()-1):
						partyAttacking=true
						partyTurn=0
						get_node("Pointer").visible=false
						get_node("MenuBG/BattleStatus").text = "The party strikes!"
						get_node("MenuBG/BattleStatus").visible = true
						
					else:
						partyTurn+=1
						get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuParty").visible=false
					menu=0
					menuOption=0
					get_node("MenuBG/Menu0/PartyBG/PartyName").text = Main.activePartyMembers[partyTurn].entName
					get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
					get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
					get_node("MenuBG/Menu0/PartyBG").rect_position = Vector2(partyMenuPos[partyTurn], -32)
					moveCursor(menu_0_offset_list[0])
				5:
					Main.activePartyMembers[partyTurn].spellChoice = menuOption
					if(Main.items[menuOption].targetParty):
						menu=7
						get_node("MenuBG/MenuParty").visible = true
						get_node("MenuBG/MenuParty/PartyName").text = Main.activePartyMembers[0].entName
						get_node("MenuBG/MenuParty/PartyHP").text = String(Main.activePartyMembers[0].HP) + "/" + String(Main.activePartyMembers[0].maxHP)
						get_node("MenuBG/MenuParty/PartyTP").text = String(Main.activePartyMembers[0].TP) + "/" + String(Main.activePartyMembers[0].maxTP)
						moveCursor(Vector2(Main.activePartyMembers[0].position.x + Main.activePartyMembers[0].cursorOffset.x, Main.activePartyMembers[0].position.y + Main.activePartyMembers[0].cursorOffset.y))
					else:
						menu=6
						get_node("MenuBG/MenuEnemy").visible = true
						get_node("MenuBG/MenuEnemy/EnemyName").text = enemies[0].entName
						get_node("MenuBG/MenuEnemy/EnemyHP").text = String(enemies[0].HP) + "/" + String(enemies[0].maxHP)
						get_node("MenuBG/MenuEnemy/EnemyTP").text = String(enemies[0].TP) + "/" + String(enemies[0].maxTP)
						moveCursor(Vector2(enemies[0].position.x + enemies[0].cursorOffset.x, enemies[0].position.y + enemies[0].cursorOffset.y))
					menuOption=0
					
					get_node("MenuBG/Menu0").visible=false
					
					get_node("MenuBG/MenuSelection").visible=false
				6:
					Main.activePartyMembers[partyTurn].action = 3
					Main.activePartyMembers[partyTurn].target = menuOption
					if(partyTurn >= Main.activePartyMembers.size()-1):
						partyAttacking=true
						partyTurn=0
						get_node("Pointer").visible=false
						get_node("MenuBG/BattleStatus").text = "The party strikes!"
						get_node("MenuBG/BattleStatus").visible = true
						
					else:
						partyTurn+=1
						get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuEnemy").visible=false
					menu=0
					menuOption=0
					get_node("MenuBG/Menu0/PartyBG/PartyName").text = Main.activePartyMembers[partyTurn].entName
					get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
					get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
					get_node("MenuBG/Menu0/PartyBG").rect_position = Vector2(partyMenuPos[partyTurn], -32)
					moveCursor(menu_0_offset_list[0])
				7:
					Main.activePartyMembers[partyTurn].action = 3
					Main.activePartyMembers[partyTurn].target = menuOption
					if(partyTurn >= Main.activePartyMembers.size()-1):
						partyAttacking=true
						partyTurn=0
						get_node("Pointer").visible=false
						get_node("MenuBG/BattleStatus").text = "The party strikes!"
						get_node("MenuBG/BattleStatus").visible = true
						
					else:
						partyTurn+=1
						get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuParty").visible=false
					menu=0
					menuOption=0
					get_node("MenuBG/Menu0/PartyBG/PartyName").text = Main.activePartyMembers[partyTurn].entName
					get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
					get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
					get_node("MenuBG/Menu0/PartyBG").rect_position = Vector2(partyMenuPos[partyTurn], -32)
					moveCursor(menu_0_offset_list[0])
				8:
					Main.activePartyMembers[partyTurn].action = miscOptions[menuOption].action
					if(partyTurn >= Main.activePartyMembers.size()-1):
						partyAttacking=true
						partyTurn=0
						get_node("Pointer").visible=false
						get_node("MenuBG/BattleStatus").text = "The party strikes!"
						get_node("MenuBG/BattleStatus").visible = true
						
					else:
						partyTurn+=1
						get_node("MenuBG/Menu0").visible=true
					get_node("MenuBG/MenuSelection").visible=false
					menu=0
					menuOption=0
					get_node("MenuBG/Menu0/PartyBG/PartyName").text = Main.activePartyMembers[partyTurn].entName
					get_node("MenuBG/Menu0/PartyBG/PartyNameHP").text = String(Main.activePartyMembers[partyTurn].HP)
					get_node("MenuBG/Menu0/PartyBG/PartyNameTech").text = String(Main.activePartyMembers[partyTurn].TP)
					get_node("MenuBG/Menu0/PartyBG").rect_position = Vector2(partyMenuPos[partyTurn], -32)
					moveCursor(menu_0_offset_list[0])
	
	animateCursor()
	
	pass

func moveCursor(var pos):
	get_node("Pointer").position = pos
	normalCursorPosition = pos

func animateCursor():
	var check = fmod(cursorTimer, 1.0)
	
	if(check <= 0.25):
		get_node("Pointer").position = normalCursorPosition
	
	if((check >= 0.26 && check <= 0.50) || (check >= 0.76)):
		get_node("Pointer").position = Vector2(normalCursorPosition.x-1, normalCursorPosition.y)
		
	if(check >= 0.51 && check <= 0.75):
		get_node("Pointer").position = Vector2(normalCursorPosition.x-2, normalCursorPosition.y)
		

func addEnemy(var ID, var x, var y):
	var enemy = BattleEntity.new(Main.enemyDefs[ID].entName, Main.enemyDefs[ID].maxHP, Main.enemyDefs[ID].maxTP, Main.enemyDefs[ID].baseAttack, Main.enemyDefs[ID].baseDefense, Main.enemyDefs[ID].baseSpecialAttack, Main.enemyDefs[ID].baseSpecialDefense, Main.enemyDefs[ID].cursorOffset.x, Main.enemyDefs[ID].cursorOffset.y)
	enemy.position = Vector2(x,y)
	var enemySpr = Sprite.new()
	enemySpr.texture = Main.enemyTex[ID]
	enemy.add_child(enemySpr)
	enemies.append(enemy)
	add_child(enemy)

func getSelection(var array, var index):
	if(array.size()>1):
		if(index==array.size()-1):
			get_node("MenuBG/MenuSelection/NextItem").visible=false
		else:
			get_node("MenuBG/MenuSelection/NextItem").visible=true
	else:
		get_node("MenuBG/MenuSelection/NextItem").visible=false
	
	if(index==0):
		get_node("MenuBG/MenuSelection/PrevItem").visible=false
	else:
		get_node("MenuBG/MenuSelection/PrevItem").visible=true
	
	if(array == Main.items):
		get_node("MenuBG/MenuSelection/CurrentItem").text = array[index].name + " x" + String(Main.items[index].quantity)
		if(index-1 >=0 && index-1<array.size()):
			get_node("MenuBG/MenuSelection/PrevItem").text = array[index-1].name + " x" + String(Main.items[index-1].quantity)
		if(index+1 >=0 && index+1<array.size()):
			get_node("MenuBG/MenuSelection/NextItem").text = array[index+1].name + " x" + String(Main.items[index+1].quantity)
	else:
		get_node("MenuBG/MenuSelection/CurrentItem").text = array[index].name
		if(index-1 >=0 && index-1<array.size()):
			get_node("MenuBG/MenuSelection/PrevItem").text = array[index-1].name
		if(index+1 >=0 && index+1<array.size()):
			get_node("MenuBG/MenuSelection/NextItem").text = array[index+1].name
	
	
	pass
