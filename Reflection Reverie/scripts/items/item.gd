extends Node

class_name Item

var mainObj
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var battleItem = true
var keyItem = false
var consumable = true

var price
var canSell = true
var quantity = 1
var value1 = 0

var targetParty = true

func setup(var itemName:String="Missing No.", var targetParty:bool=true, var price:int=0, var value1:int=0):
	self.name = itemName
	self.targetParty = targetParty
	self.price = price
	self.value1 = value1
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func use(var target):
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
