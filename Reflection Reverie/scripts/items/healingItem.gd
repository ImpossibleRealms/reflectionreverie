extends Item

#var healValue = 5
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func use(var target):
	if(quantity > 0):
		quantity -=1
		target.HP += value1
		if(target.HP > target.maxHP):
			target.HP = target.maxHP
			if(Main.inBattle):
				var notif = BattleLabel.new(Main.font1, "MAX!", target.position)
				
				Main.battle.add_child(notif)
		else:
			if(Main.inBattle):
				var notif = BattleLabel.new(Main.font1, "+" + String(value1), target.position)
				Main.battle.add_child(notif)
	else:
		if(Main.inBattle):
				var notif = BattleLabel.new(Main.font1, "GONE!", target.position)
				Main.battle.add_child(notif)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
