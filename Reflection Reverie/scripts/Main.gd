extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var battleScene = preload("res://scenes/Battle.tscn")
var battle
var inBattle
var battleWon = false
var battleType = 0

var levelSize = Vector2(160,144)

var playerSpawn = Vector2(100, 100)

var playerCanMove = true

var currentRoom = 0
var roomNames = []
var roomSizes = []

var storyProgress = 0

const dreamStartEvent = 0

var size = 6

var gameSize = Vector2(160, 144)
var windowSize = OS.get_window_size()

var font1 = BitmapFont.new()
var font1Tex = load("res://images/font.png")
var chars = " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~"

var dialogProfileImages = []
var dialogObject = load("res://prefabs/Dialog.tscn")
var dialog
var dialogInfoObject = load("res://scripts/dialog_info.gd")
var dialogs = []

var items = []
var itemDefs = []

var startTime = 0
var currentTime = 0
var savedTime = 0

var saveObject = load("res://prefabs/Save Menu.tscn")
var savebox

var miscFlags = []
var numMiscFlags = 100

var enemyDefs = []
var enemyTex = []

var spells = []
var partymembers = []
var activePartyMembers = []

var canNightmare = true
var isNightmare = false

var random

var learnset = []
var learnsetObject = load("res://scripts/learnset_info.gd")

var musicPlayer
var battleMusicPlayer
var intendedVolume = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	random = RandomNumberGenerator.new()
	
	for i in range(numMiscFlags):
		miscFlags.append(false)
	
	startTime = OS.get_system_time_secs()
	resizeScreen(size)
	
	loadRoomData()
	loadEnemies()
	loadItems()
	loadSpells()
	loadLearnsetData()
	loadProfiles()
	loadDialogInfo()
	loadParty()
	
	font1.add_texture(font1Tex)
	for i in range (0, chars.length()):
		font1.add_char(chars.ord_at(i), 0, Rect2(8 * (i%16), 8 * (i/16), 8, 8), Vector2(0, 0), 8)
	
	musicPlayer = AudioStreamPlayer.new()
	musicPlayer.stream = load("res://music/ow_garden.ogg")
	add_child(musicPlayer)
	
	battleMusicPlayer = AudioStreamPlayer.new()
	battleMusicPlayer.stream = load("res://music/battle_standardWIP.ogg")
	add_child(battleMusicPlayer)
	
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(musicPlayer.volume_db < intendedVolume):
		musicPlayer.volume_db += 0.1
	if(musicPlayer.volume_db > intendedVolume):
		musicPlayer.volume_db = intendedVolume
	currentTime = OS.get_system_time_secs()
	if(Input.is_action_just_pressed("screenshot")):
		var directory = Directory.new();
		if(!directory.file_exists("user://screenshots")):
			directory.make_dir("user://screenshots")
		var result = get_viewport().get_texture().get_data()
		result.flip_y()
		result.save_png("user://screenshots/screenshot" + "_" + str(OS.get_datetime()["year"]) + "_" + str(OS.get_datetime()["month"]) + "_" + str(OS.get_datetime()["day"]) + "_" + str(OS.get_datetime()["hour"]) + str(OS.get_datetime()["minute"]) + str(OS.get_datetime()["second"]) + ".png")
#	pass

func loadRoom(var ID, var playerPos):
	#var room = load("res://scenes/OverworldRooms/room" + String(ID) + ".tscn")
	#get_tree().change_scene_to(room)
	get_tree().change_scene("res://scenes/OverworldRooms/room" + String(ID) + ".tscn")
	currentRoom = ID
	playerSpawn = playerPos
	#var player = room.get_node("OverworldPlayer")
	#print(player)
	#get_node("OverworldPlayer/Camera2D").limit_right = roomSizes[ID].x*8
	#player.get_node("Camera2D").limit_bottom = roomSizes[ID].y*8

func loadRoomData():
	var file = File.new()
	if(file.file_exists("res://defs/room_info.csv")):
		file.open("res://defs/room_info.csv", File.READ)
		var roomDef = file.get_csv_line("|")
		while(file.get_position() < file.get_len()):
			roomDef = file.get_csv_line("|")
			roomNames.append(roomDef[0])
			roomSizes.append(Vector2(int(roomDef[2]), int(roomDef[3])))
	file.close()

func loadDialogInfo():
	var file = File.new()
	if(file.file_exists("res://defs/dialog_text.csv")):
		file.open("res://defs/dialog_text.csv", File.READ)
		var dialogDef = file.get_csv_line("|")
		while(file.get_position() < file.get_len()):
			dialogDef = file.get_csv_line("|")
			dialogDef[1] = dialogDef[1].replace("[a]", "'")
			dialogDef[1] = dialogDef[1].replace("[q]", "\"")
			var dialog = dialogInfoObject.new()
			dialog.setup(int(dialogDef[0]), dialogDef[1], int(dialogDef[2]), int(dialogDef[3]), int(dialogDef[4]), int(dialogDef[5]))
			dialogs.append(dialog)
	file.close()
	pass

func loadProfiles():
	var file = File.new()
	if(file.file_exists("res://defs/portraits.csv")):
		file.open("res://defs/portraits.csv", File.READ)
		var profDef = file.get_csv_line("|")
		while(file.get_position() < file.get_len()):
			profDef = file.get_csv_line("|")
			dialogProfileImages.append(load("res://images/profiles/" + profDef[0]))
	file.close()

func resizeScreen(var value):
	OS.set_window_size(gameSize*value);
	windowSize = OS.get_window_size()
	OS.set_window_position(OS.get_screen_size()*0.5 - (gameSize*value)*0.5)

func createDialog(var startingDialog):
	Main.playerCanMove = false
	dialog = dialogObject.instance()
	#print(dialogs[startingDialog].text)
	#print(get_tree().get_current_scene())
	get_tree().get_current_scene().get_node("OverworldPlayer/UI").add_child(dialog)
	dialog.changeDialog(dialogs[startingDialog].text, dialogs[startingDialog].profile)
	dialog.currentDialogID = startingDialog
	dialog.position = Vector2(0, 112)
	pass

func changeDialog():
	if(dialogs[dialog.currentDialogID].next1 == -64):
		dialog.queue_free()
		savebox = saveObject.instance()
		savebox.isSaving = true
		get_tree().get_current_scene().get_node("OverworldPlayer/UI").add_child(savebox)
	elif(dialogs[dialog.currentDialogID].next1 < 0):
		dialog.queue_free()
		Main.playerCanMove = true
	else:
		dialog.currentDialogID = dialogs[dialog.currentDialogID].next1
		dialog.changeDialog(dialogs[dialog.currentDialogID].text, dialogs[dialog.currentDialogID].profile)
	pass

func openChest(var itemID, var progress, var NPC, var miscFlagID):
	Main.playerCanMove = false
	dialog = dialogObject.instance()
	get_tree().get_current_scene().get_node("OverworldPlayer/UI").add_child(dialog)
	dialog.isHardcoded = true
	if(miscFlags[miscFlagID] == true):
		progress = 1
		NPC.progress = 1
	
	if(progress == 0):
		dialog.changeDialog("Got " + itemDefs[itemID].name + "!", -1)
		addItem(itemDefs[itemID])
		NPC.progress = 1
		miscFlags[miscFlagID] = true
	else:
		dialog.changeDialog("It's empty.", -1)
		#print(items.size())
	dialog.position = Vector2(0, 112)
	pass

func addItem(var item):
	var newItem = true
	for i in range(items.size()):
		if(item.name == items[i].name):
			newItem = false
			items[i].quantity += 1
			if(items[i].quantity > 99):
				items[i].quantity = 99
	
	if(newItem):
		item.quantity = 1
		items.append(item)

func loadItems():
	var file = File.new()
	
	var scriptNames = []
	var scripts = []
	var scriptToUse
	var scriptExists = false
	
	if(file.file_exists("res://defs/items.csv")):
		file.open("res://defs/items.csv", File.READ)
		var itemDef = file.get_csv_line("|")
		while(file.get_position() < file.get_len()):
			scriptExists = false
			itemDef = file.get_csv_line("|")
			for i in range(scriptNames.size()):
				if (scriptNames[i] == itemDef[3]):
					scriptExists = true
					scriptToUse = scripts[i]
			
			if(!scriptExists):
				scriptNames.append(itemDef[3])
				scripts.append(load("res://scripts/items/" + itemDef[3]))
				scriptToUse = scripts[scripts.size()-1]
			
			var item = scriptToUse.new()
			item.setup(itemDef[0], bool(itemDef[1]), int(itemDef[2]), int(itemDef[4]))
			itemDefs.append(item)
		file.close()
			#roomSizes.append(Vector2(int(roomDef[2]), int(roomDef[3])))
	#var itemScr = load("res://scripts/items/healingItem.gd")
	#var item = itemScr.new()
	#item.setup("Morning Dew", true, 0)
	#item.mainObj = self
	#items.append(item)
	
	#item = itemScr.new()
	#item.setup("Test 2", false, 0)
	#item.mainObj = self
	#items.append(item)
	#item = itemScr.new()
	#item.setup("Test 3", false, 0)
	#item.mainObj = self
	#items.append(item)
	#item = itemScr.new()
	#item.setup("Test 4", false, 0)
	#item.mainObj = self
	#items.append(item)
	pass

func loadEnemies():
	var file = File.new()
	if(file.file_exists("res://defs/enemies_battle.csv")):
		file.open("res://defs/enemies_battle.csv", File.READ)
		var enemyDef = file.get_csv_line("|")
		while(file.get_position() < file.get_len()):
			enemyDef = file.get_csv_line("|")
			var enemy = BattleEntity.new(enemyDef[0], int(enemyDef[2]), int(enemyDef[3]), int(enemyDef[4]), int(enemyDef[5]), int(enemyDef[6]), int(enemyDef[7]), int(enemyDef[8]), int(enemyDef[9]))
			enemyDefs.append(enemy)
			var enemyTex = load("res://images/battle/Enemies/" + enemyDef[1])
			self.enemyTex.append(enemyTex)
		file.close()

func loadSpells():
	var file = File.new()
	if(file.file_exists("res://defs/spells.csv")):
		file.open("res://defs/spells.csv", File.READ)
		var spellDef = file.get_csv_line("|")
		while(file.get_position() < file.get_len()):
			spellDef = file.get_csv_line("|")
			var spellScr = load("res://scripts/spells/" + spellDef[1])
			var spell = spellScr.new()
			spell.setup(spellDef[0], int(spellDef[3]), bool(spellDef[4]))
			#print("Does " + spellDef[0] + " target the party? " + String(bool(spellDef[4])))
			spell.level = int(spellDef[2])
			spell.nightmare = bool(spellDef[5])
			spell.mainObj = self
			spells.append(spell)
	file.close()
	#var spellScr = load("res://scripts/spells/heal.gd")
	#var spell = spellScr.new()
	

func loadLearnsetData():
	var file = File.new()
	if(file.file_exists("res://defs/party_learnset.csv")):
		file.open("res://defs/party_learnset.csv", File.READ)
		var learnDef = file.get_csv_line("|")
		while(file.get_position() < file.get_len()):
			learnDef = file.get_csv_line("|")
			var learn = learnsetObject.new()
			learn.setup(int(learnDef[0]), int(learnDef[1]), int(learnDef[2]), bool(learnDef[3]))
			learnset.append(learn)
			#roomNames.append(roomDef[0])
			#roomSizes.append(Vector2(int(roomDef[2]), int(roomDef[3])))
	file.close()

func loadParty():
	var file = File.new()
	if(file.file_exists("res://defs/party_members.csv")):
		file.open("res://defs/party_members.csv", File.READ)
		var partyDef = file.get_csv_line("|")
		var member
		var memberID = 0
		while(file.get_position() < file.get_len()):
			partyDef = file.get_csv_line("|")
			member = PartyMember.new(partyDef[0])
			member.sprite = partyDef[2]
			member.LV = int(partyDef[1])
			member.HP = int(partyDef[3])
			member.maxHP = int(partyDef[3])
			member.TP = int(partyDef[4])
			member.maxTP = int(partyDef[4])
			member.attack = int(partyDef[5])
			member.baseAttack = int(partyDef[5])
			member.defense = int(partyDef[6])
			member.baseDefense = int(partyDef[6])
			member.specialAttack = int(partyDef[7])
			member.baseSpecialAttack = int(partyDef[7])
			member.specialDefense = int(partyDef[8])
			member.baseSpecialDefense = int(partyDef[8])
			member.cursorOffset = Vector2(float(partyDef[9]), float(partyDef[10]))
			
			for learn in learnset:
				if(learn.partyID == memberID):
					if(learn.unlockLV <= member.LV):
						member.spells.append(spells[learn.spellID])
			
			partymembers.append(member)
			memberID += 1
			
	file.close()
	activePartyMembers.append(partymembers[0])
	activePartyMembers.append(partymembers[1])
	activePartyMembers.append(partymembers[2])
	#var member = PartyMember.new("David")
	#member.battleMain = self
	#var placeholder = ColorRect.new()
	#member.position = Vector2(72, 84)
	#placeholder.color = Color(0.203922, 0.407843, 0.337255)
	#placeholder.rect_size = Vector2(16,24)
	#member.add_child(placeholder)
	#partymembers.append(member)
	
	
	#member = PartyMember.new("Faith")
	#member.battleMain = self
	#placeholder = ColorRect.new()
	#member.position = Vector2(16, 80)
	#placeholder.color = Color(0.203922, 0.407843, 0.337255)
	#placeholder.rect_size = Vector2(16,24)
	#member.add_child(placeholder)
	#partymembers.append(member)
	
	#member.spells.append(Main.spells[0])
	#member.spells.append(Main.spells[0])
	#member.spells.append(Main.spells[0])
	#member.spells.append(Main.spells[0])
	#member.spells.append(Main.spells[0])
	
	#print("number of spells is " + String(member.spells.size()))
	
	#get_node("Party").add_child(member)
	
	#member = PartyMember.new("Benny")
	#member.battleMain = self
	#placeholder = ColorRect.new()
	#member.position = Vector2(128, 80)
	#placeholder.color = Color(0.203922, 0.407843, 0.337255)
	#placeholder.rect_size = Vector2(16,24)
	#member.add_child(placeholder)
	#partymembers.append(member)
	#get_node("Party").add_child(member)

func startBattle():
	#for party in activePartyMembers:
	#	party.HP = 1
	musicPlayer.stream_paused = true
	battleMusicPlayer.playing = true
	#print("This will start a battle when I learn how to code, lol")
	battleScene = load("res://scenes/Battle.tscn")
	battle = battleScene.instance()
	inBattle = true
	get_tree().get_current_scene().get_node("OverworldPlayer/UI").add_child(battle)
	pass

func endBattle():
	musicPlayer.volume_db = -30
	musicPlayer.stream_paused = false
	battleMusicPlayer.playing = false
	battle.queue_free()
#func booleanComp(var input):
#	if(input.equal)
