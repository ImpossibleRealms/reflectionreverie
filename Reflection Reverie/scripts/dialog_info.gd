extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var profile = -1
var text = "Missing."
var next1 = -1
var next2 = -1
var next3 = -1
var next4 = -1

func setup(var profile, var text, var next1, var next2, var next3, var next4):
	self.profile = profile
	self.text = text
	self.next1 = next1
	self.next2 = next2
	self.next3 = next3
	self.next4 = next4
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
