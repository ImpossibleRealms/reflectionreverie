extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var mainMenuOpen = false
var saveMenuOpen = false
var cursorPositions = [Vector2(47, 96), Vector2(47, 112), Vector2(31, 128)]
var option = 0

var saveMenu
var cursorTimer = 0
var normalCursorPosition

# Called when the node enters the scene tree for the first time.
func _ready():
	normalCursorPosition = get_node("Pointer").position
	get_node("Title").add_font_override("font", Main.font1)
	get_node("Build").add_font_override("font", Main.font1)
	get_node("Start").add_font_override("font", Main.font1)
	get_node("New").add_font_override("font", Main.font1)
	get_node("Load").add_font_override("font", Main.font1)
	get_node("Size").add_font_override("font", Main.font1)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	cursorTimer += delta
	
	if(mainMenuOpen == false):
		if(Input.is_action_just_pressed("ui_accept")):
			mainMenuOpen = true
			get_node("Start").visible = false
			get_node("New").visible = true
			get_node("Load").visible = true
			get_node("Size").visible = true
			moveCursor(cursorPositions[option])
	elif(!saveMenuOpen):
		if(Input.is_action_just_pressed("ui_up")):
			if(option == 0):
				option = cursorPositions.size()-1
			else:
				option -= 1
			moveCursor(cursorPositions[option])
		elif(Input.is_action_just_pressed("ui_down")):
			if(option == cursorPositions.size()-1):
				option = 0
			else:
				option += 1
			moveCursor(cursorPositions[option])
		
		if(option == 2):
			if(Input.is_action_just_pressed("ui_left")):
				if(Main.size > 1):
					Main.size -= 1
					get_node("Size").text = "< Size x" + String(Main.size) + " >"
					Main.resizeScreen(Main.size)
			elif(Input.is_action_just_pressed("ui_right")):
					Main.size += 1
					get_node("Size").text = "< Size x" + String(Main.size) + " >"
					Main.resizeScreen(Main.size)
		
		if(Input.is_action_just_pressed("ui_accept")):
			if(option==0):
				Main.musicPlayer.playing = true
				Main.loadRoom(0, Vector2(100, 100))
			elif(option==1):
				saveMenu = Main.saveObject.instance()
				add_child(saveMenu)
				get_node("Pointer").visible = false
				saveMenuOpen = true
	else:
		if(saveMenu.currentItem == 3 && Input.is_action_just_pressed("ui_accept")):
			get_node("Pointer").visible = true
			saveMenuOpen = false
	
	animateCursor()
#	pass

func moveCursor(var pos):
	get_node("Pointer").position = pos
	normalCursorPosition = pos

func animateCursor():
	var check = fmod(cursorTimer, 1.0)
	
	if(check <= 0.25):
		get_node("Pointer").position = normalCursorPosition
	
	if((check >= 0.26 && check <= 0.50) || (check >= 0.76)):
		get_node("Pointer").position = Vector2(normalCursorPosition.x-1, normalCursorPosition.y)
		
	if(check >= 0.51 && check <= 0.75):
		get_node("Pointer").position = Vector2(normalCursorPosition.x-2, normalCursorPosition.y)
