extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export var roomID = 0
export var loadCoords = Vector2(0.0, 0.0)

# Called when the node enters the scene tree for the first time.
func _ready():
	set_monitoring(true)
	connect("body_entered", self, "warpPlayer")
	pass # Replace with function body.

func warpPlayer(var body):
	#print(body.name)
	if(body.name == "OverworldPlayer"):
		Main.loadRoom(roomID, loadCoords)
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
