extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	set_monitoring(true)
	connect("body_entered", self, "objectDetected")
	connect("body_exited", self, "objectLeft")
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func objectDetected(var body):
	if(body.name == "OverworldPlayer"):
		#print(get_parent())
		get_parent().canInteract=true
		get_parent().inRange=true
	pass

func objectLeft(var body):
	if(body.name == "OverworldPlayer"):
		get_parent().canInteract=false
		get_parent().inRange=false
	pass
