extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var velocity = Vector2(0,0)

var speed = 80
var walkSpeed = 80
var runSpeed = 120

var walkTimer = 0

var walkDir = 0

var sprite

# Called when the node enters the scene tree for the first time.
func _ready():
	sprite = get_node("Sprite")
	position = Main.playerSpawn
	get_node("Camera2D").limit_right = Main.roomSizes[Main.currentRoom].x*8
	get_node("Camera2D").limit_bottom = Main.roomSizes[Main.currentRoom].y*8
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(Main.playerCanMove):
		move_and_slide(velocity)
		if(Input.is_action_pressed("ui_up") || Input.is_action_pressed("ui_down") || Input.is_action_pressed("ui_left") || Input.is_action_pressed("ui_right")):
			walkTimer += delta
		else:
			walkTimer = 0
	
		if(Input.is_action_pressed("ui_left") && !Input.is_action_pressed("ui_right")):
			walkDir = 3
			velocity.x = -speed
		
		if(!Input.is_action_pressed("ui_left") && Input.is_action_pressed("ui_right")):
			walkDir = 1
			velocity.x = speed
		
		if((Input.is_action_pressed("ui_left") && Input.is_action_pressed("ui_right")) || (!Input.is_action_pressed("ui_left") && !Input.is_action_pressed("ui_right"))):
			velocity.x = 0
		
		if(Input.is_action_pressed("ui_up") && !Input.is_action_pressed("ui_down")):
			walkDir = 2
			velocity.y = -speed
		
		if(!Input.is_action_pressed("ui_up") && Input.is_action_pressed("ui_down")):
			walkDir = 0
			velocity.y = speed
		
		if((Input.is_action_pressed("ui_up") && Input.is_action_pressed("ui_down")) || (!Input.is_action_pressed("ui_up") && !Input.is_action_pressed("ui_down"))):
			velocity.y = 0
		walk()
	else:
		sprite.set_region_rect(Rect2(0, 16*walkDir, 16, 16))
		walkTimer = 0
	if(Input.is_action_pressed("ui_cancel")):
		speed=runSpeed
	else:
		speed=walkSpeed
	
	if(position.x<=5):
		position.x=5
	
	if(position.x>=get_node("Camera2D").limit_right-5):
		position.x=get_node("Camera2D").limit_right-5
	
	if(position.y<=8):
		position.y=8
	
	if(position.y>=get_node("Camera2D").limit_bottom-8):
		position.y=get_node("Camera2D").limit_bottom-8
	
#	pass

func walk():
	if(fmod(walkTimer, 0.6) > 0.15 && fmod(walkTimer, 0.6) <= 0.3):
		sprite.set_region_rect(Rect2(16, 16*walkDir, 16, 16))
	elif(fmod(walkTimer, 0.6) > 0.3 && fmod(walkTimer, 0.6) <= 0.45):
		sprite.set_region_rect(Rect2(32, 16*walkDir, 16, 16))
	elif(fmod(walkTimer, 0.6) > 0.45):
		sprite.set_region_rect(Rect2(48, 16*walkDir, 16, 16))
	else:
		sprite.set_region_rect(Rect2(0, 16*walkDir, 16, 16))
	pass
