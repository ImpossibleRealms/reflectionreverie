extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var text = "test"
var dialogObject
var profileObject
var currentDialogID = 0

var isHardcoded = false

# Called when the node enters the scene tree for the first time.
func _ready():
	dialogObject = get_node("Text")
	dialogObject.add_font_override("font", Main.font1)
	profileObject = get_node("Profile")
	#print(profileObject)
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(Input.is_action_just_pressed("ui_accept")):
		if(!isHardcoded):
			Main.changeDialog()
		else:
			queue_free()
			Main.playerCanMove = true
	pass
	
func changeDialog(var text, var profile):
	if(profile<0 || profile >= Main.dialogProfileImages.size()):
		profileObject.visible=false
		#print(Main.dialogProfileImages.size())
	else:
		profileObject.visible=true
		profileObject.texture = Main.dialogProfileImages[profile]
	dialogObject.text = text
	pass
