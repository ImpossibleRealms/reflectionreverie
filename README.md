Hello, if you're reading this, you're interested in the source code to my game, Reflection Reverie for some reason. Well, I guess I can at least point you in the right direction.

To make use of this, you're going to need the Godot Game Engine version 3.5-stable or later. All of the text and party definitions are in .csv files with the "|" symbol as a delimiter. ' and " can't be used in text for some reason, so you'll need to replace each with [a] and [q] respectively.

Finally, there is cut content from the GBJam build in here, some of which might be spoilers. You have been warned.
